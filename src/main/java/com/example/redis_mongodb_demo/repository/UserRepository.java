package com.example.redis_mongodb_demo.repository;

import com.example.redis_mongodb_demo.entity.User;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertOneResult;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {
    @Autowired
    private MongoDatabase mongoDatabase;


    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    private MongoCollection<User> mongoCollection ;

    @Autowired
    public void user() {
        mongoCollection = mongoDatabase.getCollection("user", User.class);
    }

    public User create(User user) {
        User rs = null;
        try {
            redisTemplate.opsForValue().set(user.getCode(), user);
            rs = mongoTemplate.insert(user);
            System.out.println("########## " + redisTemplate.opsForValue().get(rs.getCode()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (rs == null){
            return null;
        }
        return rs;
    }

    public Object findOne(String code){
        Object rs = null;
        try {
            rs = redisTemplate.opsForValue().get(code);
            if(rs == null){
                Query query = new Query();
                query.addCriteria(Criteria.where("code").is(code));

                System.out.println("########## " +redisTemplate.opsForValue().get(code));
                User user = mongoTemplate.findOne(query, User.class);
                if (user == null){
                    return null;
                }else {
                    return user;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return rs;
    }

    public Boolean deleteCache(String code){
        redisTemplate.delete(code);
        return true;
    }

}

package com.example.redis_mongodb_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableCaching
@EnableMongoRepositories
public class RedisMongodbDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisMongodbDemoApplication.class, args);
	}

}

package com.example.redis_mongodb_demo.entity;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@Document(collection = "user")
public class User implements  Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    private ObjectId id;
    private String code;
    private String name;
    private String address;
}

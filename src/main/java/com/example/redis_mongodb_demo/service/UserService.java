package com.example.redis_mongodb_demo.service;

import com.example.redis_mongodb_demo.entity.User;
import com.example.redis_mongodb_demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    public Object findOne(String code){
        return repository.findOne(code);
    }

    public User create(User user){
        return repository.create(user);
    }

    public Boolean deleteRedis(String code){
        return repository.deleteCache(code);
    }
}

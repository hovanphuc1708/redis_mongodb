package com.example.redis_mongodb_demo.controller;

import com.example.redis_mongodb_demo.entity.User;
import com.example.redis_mongodb_demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService service;

    @GetMapping("/get")
    public Object findOne(@RequestParam(value = "code") String code){
        Object user = service.findOne(code);
        if (user == null){
            return null;
        }
        return user;
    }

    @PostMapping("/create")
    public User create(@RequestBody User user){
        User rs = service.create(user);
        if (rs == null){
            return null;
        }
        return rs;
    }

    @DeleteMapping("/delete")
    public String deleteRedis(@RequestParam(value = "code")String code){
        Boolean check = service.deleteRedis(code);
        if (!check){
            return "False";
        }
        return "OKE";
    }
}
